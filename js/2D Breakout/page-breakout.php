<?php
/**
 * Template Name: 2D Breakout
 */
?>

<style>
	#breakout {
		display: block;
		margin-left: auto;
		margin-right: auto;
	}
</style>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <div class="container">
 		<div class="row">
 			<div class="col-md-12">
 				<h1><?php the_title(); ?></h1>
 			</div>
 		</div>
  </div>
<?php endwhile; ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="breakout-container">
				<canvas id="breakout" width="480px" height="320px"></canvas>
			</div>
		</div>
	</div>
</div>

<script>

	var canvas = document.getElementById('breakout');
	var ctx = canvas.getContext('2d');

	var x = canvas.width/2;
	var y = canvas.height-30;

	var dx = 4;
	var dy = -2;

	var ballRadius = 10;

	var paddleHeight = 10;
	var paddleWidth = 75;
	var paddleX = (canvas.width - paddleWidth)/2;

	var rightPressed = false;
	var leftPressed = false;

	var brickRowCount = 4;
	var brickColumnCount = 5;
	var brickWidth = 75;
	var brickHeight = 20;
	var brickPadding = 10;
	var brickOffsetTop = 30;
	var brickOffsetLeft = 30;

	var score = 0;

	document.addEventListener('keydown', keyDownHandler, false);
	document.addEventListener('keyup', keyUpHandler, false);

	var bricks = [];
	for (c = 0; c < brickColumnCount; c++) {
		bricks[c] = [];
		for (r = 0; r < brickRowCount; r++) {	
			bricks[c][r] = { x: 0, y: 0, status: 1 };
		}
	}

	function keyDownHandler(e) {
		if (e.keyCode == 39) {
			rightPressed = true;
		}
		else if (e.keyCode == 37) {
			leftPressed = true;
		}
	}

	function keyUpHandler(e) {
		if (e.keyCode == 39) {
			rightPressed = false;
		}
		else if (e.keyCode == 37) {
			leftPressed = false;
		}
	}

	function drawBall() {
		ctx.beginPath();
		ctx.arc(x, y, ballRadius, 0, Math.PI*2);
		ctx.fillStyle = "#0095DD";
		ctx.fill();
		ctx.closePath();
	}

	function drawPaddle() {
		ctx.beginPath();
		ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
		ctx.fillStyle = "#0095DD";
		ctx.fill();
		ctx.closePath();
	}

	function drawBricks() {
		for(c=0; c<brickColumnCount; c++) {
			for(r=0; r<brickRowCount; r++) {
				if(bricks[c][r].status == 1) {
					var brickX = (c*(brickWidth+brickPadding))+brickOffsetLeft;
					var brickY = (r*(brickHeight+brickPadding))+brickOffsetTop;
					bricks[c][r].x = brickX;
					bricks[c][r].y = brickY;
					ctx.beginPath();
					ctx.rect(brickX, brickY, brickWidth, brickHeight);
					ctx.fillStyle = "#0095DD";
					ctx.fill();
					ctx.closePath();
				}
			}
		}
	}

	function collisionDetection() {
		for(c = 0; c < brickColumnCount; c++) {
			for(r = 0; r < brickRowCount; r++) {
				var b = bricks[c][r];
				if (b.status == 1) {
					if ( x > b.x && x < b.x +brickWidth && y > b.y && y < b.y + brickHeight) {
						dy = -dy;
						b.status = 0;
						score++;
						if(score == brickRowCount*brickColumnCount) {
							alert("Du har för mycket fritid.");
							document.location.reload();
						}
					}
				}
			}
		}
	}

	function drawScore() {
		ctx.font = '16px Arial';
		ctx.fillStyle = "#333333";
		ctx.fillText('Poäng: ' + score, 8, 20);
	}

	function draw() {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		
		drawBricks();
		drawBall();
		drawPaddle();
		drawScore();
		collisionDetection();

		x += dx;
		y += dy;

		if ( x + dx > canvas.width - ballRadius || x + dx < ballRadius ) {
			dx = -dx;
		}

		if (y + dy < ballRadius ) {
			dy = -dy;
		}
		else if ( y + dy > canvas.height - ballRadius ) {
			if ( x > paddleX && x < paddleX + paddleWidth) {
				dy = -dy;
			}
			else {
				alert('Ta en öl istället');
				document.location.reload();				
			}
		}

		if (rightPressed && paddleX < canvas.width - paddleWidth) {
			paddleX += 7;
		}
		else if (leftPressed && paddleX > 0) {
			paddleX -= 7;
		}
	}

	setInterval(draw, 10);

</script>
