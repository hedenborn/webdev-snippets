/**
 * scrollTo()
 * 
 * ScrollTo scrolls to a given element in a given time
 * ScrollTo handlers calls the function onload if a hash is present and 
 * when a linked is clicked if the hash exists on the same page
 * 
 * @param $ele Element too scroll to 
 * @param $time Scroll time in ms
 * 
 * @return scrolls to element
 *
 * @version  1.0
 */

function scrollTo( $ele, $time ){
	var $offset = $ele.offset().top;
	$("html, body").animate({
		scrollTop: $offset+'px' 
	}, $time );
}

//ScrollTo handlers

var $hash = window.location.hash;

//If a link contains a hash and it exists on the same page, scroll to it
$( 'a' ).on( 'click', function( e ){

	var $target = $( this );

	//Do nothing if the link is a part of a no-scroll element
	if( !($target.parents( '.no-scroll' ).length) && !($target.hasClass('no-scroll')) ){

		//Get the a-tag if target is a child to the a-tag (This seams to be necessary if the link contains an svg-obj)
		if( !( $target.is( 'a' )) ){
			$target = $target.closest( 'a' );
		}

		//Check for hash, prevent default if found	
		var $href = $target.attr( 'href' ),
			$hrefIndex = $href.indexOf('#');

		//If link contains hash
		if( $hrefIndex >= 0 ){

			var $hrefLength = $href.length,
				$hash = $href.substring( $hrefIndex, $hrefLength );

			//If hash exists on this page
			if( $( $hash ).length ){
				e.preventDefault();
				scrollTo( $( $hash ), 500 );
			}
		}
	}
});

//Scrolls to hash on page load if it exists
if( $hash.length > 0 ){
	scrollTo( $( $hash ), 1500 );
}

//<!--ScrollTo handlers End-->