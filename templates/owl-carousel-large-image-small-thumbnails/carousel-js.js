var mainCarousel = $("#main-carousel");
var navCarousel = $("#nav-carousel");
var slidesPerPage = 6; //globaly define number of elements per page
var syncedSecondary = true;

function syncPosition(el) {
  //if you set loop to false, you have to restore this next line
    //var current = el.item.index;
  //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - 0.5);
    if(current < 0) {
      current = count;
    }
    if(current > count) {
      current = 0;
    }
  //end block

  navCarousel.find(".owl-item").removeClass("current").eq(current).addClass("current");

  var onscreen = navCarousel.find('.owl-item.active').length - 1;
  var start = navCarousel.find('.owl-item.active').first().index();
  var end = navCarousel.find('.owl-item.active').last().index();

  if (current > end) {
    navCarousel.data('owl.carousel').to(current, 100, true);
  }
  if (current < start) {
    navCarousel.data('owl.carousel').to(current - onscreen, 100, true);
  }
}

function syncPosition2(el){
  if(syncedSecondary) {
    var number = el.item.index;
      mainCarousel.data('owl.carousel').to(number, 100, true);
  }
}

mainCarousel.owlCarousel({
  slideSpeed : 2000,
  autoHeight : true,
  autoplay : false,
  items : 1,
  dots : false,
  loop : true,
  responsiveRefreshRate : 200,
}).on('changed.owl.carousel', syncPosition);

navCarousel.on('initialized.owl.carousel', function () {
   navCarousel.find(".owl-item").eq(0).addClass("current");
}).owlCarousel({
  responsiveClass:true,
    responsive:{
        0:{
            items:3,
        },
        544:{
            items:4,
        },
        1200:{
            items: 5,
        }
    },
    slideBy: 1,
  slideSpeed : 500,
  smartSpeed: 200,
  dots: false,
  responsiveRefreshRate : 200
}).on('changed.owl.carousel', syncPosition2);

navCarousel.on("click", ".owl-item", function(e){
  e.preventDefault();
  var number = $(this).index();
  mainCarousel.data('owl.carousel').to(number, 300, true);
});

$( '#next-btn' ).on( 'click', function(){
  mainCarousel.trigger('next.owl.carousel');
});