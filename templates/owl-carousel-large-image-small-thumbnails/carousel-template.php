<div class="main-carousel-container">

    <?php // Only activate the carousel if there are more than one image ?>
    <?php if( count( $images_array ) > 1 ) : ?>

        <div id="main-carousel" class="carousel owl-carousel owl-theme main-carousel">
            <?php foreach( $images_array as $key => $image ) : ?>

                <div class="image-container">
                    <img src="<?= $image['url']; ?>" alt="<?= $image['alt'] ?>">
                </div>

            <?php endforeach; ?>
        </div>

    <?php elseif ( count( $images_array ) == 1 )  : ?>

        <div class="image-container">
            <img src="<?= $images_array[0]['url']; ?>" alt="<?= $images_array[0]['alt'] ?>">
        </div>

    <?php endif; ?>

</div>

<?php // Only show the navigation-carousel if there are more than one image ?>
<?php if( count( $images_array ) > 1 ) : ?>
    <div class="nav-carousel-container">
        
        <div id="nav-carousel" class="carousel owl-carousel owl-theme nav-carousel">
            <?php foreach( $images_array as $key => $image ) : ?>

                <div class="image-container">
                    <img src="<?= $image['url']; ?>" alt="<?= $image['alt'] ?>">
                </div>

            <?php endforeach; ?>
        </div>

        <div id="next-btn" class="next-btn">
            <div class="row top"></div>
            <div class="row bot"></div>
        </div>

    </div>
<?php endif; ?>