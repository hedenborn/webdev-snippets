<?php
/**
 * get_automatic_page_walker()
 * 
 * Get all pages and return them in an orderd array that can detect 
 * current page, if a child is current page and if page has children.
 * get_automatic_page_walker() uses sub_menu_builder()
 *
 * @param $pageID used to detect current page
 * 
 * @return returns an order array
 *
 * @version 1.2
 */ 

function sub_menu_builder( $page, $current_page_id ){

	//Variables
		$current = ( $page->ID === $current_page_id ) ? true : false;
		$children = false;
		$active = false;

	//Check if page has sub_pages
		$args = array(
			'sort_order' => $sort_order,
			'sort_column' => $sort_column,
			'child_of' => $page->ID,
			'parent' => $page->ID,
			'post_type' => 'page',
			'post_status' => 'publish'
		); 
		$sub_pages = get_pages($args);

		if( $sub_pages ) : 

			foreach( $sub_pages  as $sub_page ) : 
				$page_branch = sub_menu_builder( $sub_page, $current_page_id );

				if( $page_branch ) : 
					$children[] = $page_branch[0];

					if( !$active ) :
						$active = $page_branch[1];
					endif;

				endif;

			endforeach;

		endif;

	//Save info about page
		$menu_item = array(
			'title' 		=> get_the_title( $page->ID  ),
			'link' 			=> ( $page->ID ) ? get_the_permalink( $page->ID ) : '',
			'current'		=> $current,
			'active'		=> ( $current || $active ) ? true : false,
			'children' 		=> $children,
		);

	//Return
		$value = array(
			0 => $menu_item,
			1 => $menu_item['active'],
		);
		return $value;

}//!sub_menu_builder()

function get_automatic_page_walker( $pageID = false ){

	//Variables
	$sort_order = 'desc';
	$sort_column = 'menu_order';
	$current_page_id = get_the_ID();

	//Creates the array to return
	$menu_array = array();

	//Get all top-level pages
	$args = array(
		'sort_order' => $sort_order,
		'sort_column' => $sort_column,
		'parent' => 0,
		'post_type' => 'page',
		'post_status' => 'publish'
	); 
	$pages = get_pages($args); 

	if( $pages ) :

		foreach( $pages as $page ) : 

			$page_branch = sub_menu_builder( $page, $current_page_id );

			if( $page_branch ){

				$menu_array[] = $page_branch[0];
			}

		endforeach;

		if( $menu_array ) :
			return $menu_array;

		else :
			return false;

		endif; 

	else : 
		return false;

	endif;

}//!get_automatic_page_walker()