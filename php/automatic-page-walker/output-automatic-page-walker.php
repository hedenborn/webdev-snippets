<?php 
/**
 * output_main_navigation()
 * 
 * Uses get_automatic_page_walker() to build a html menu
 * output_main_navigation() uses output_navigation_item()
 *
 * @param $pageID used by get_automatic_page_walker() to detect current page
 * 
 * @return outputs an html menu
 *
 * @version 1.2
 */ 

function output_navigation_item( $menu_item ){

	$classes = '';
	$classes .= ( $menu_item['current'] ) ? ' current' : '';
	$classes .= ( $menu_item['children'] ) ? ' has-children' : '';
	$classes .= ( $menu_item['active'] ) ? ' active' : '';
	?>
		<li class="menu-item<?= $classes ?>">
			<a href="<?= $menu_item['link'] ?>" class="handle-menu">
				<?= $menu_item['title'] ?>
			</a>

			<?php if( $menu_item['children'] ) : ?>

				<div class="close-btn-container">
					<div class="close-btn">
						<div class="menu-row top"></div>
						<div class="menu-row bot"></div>
					</div>
				</div>

				<ul class="sub-menu">
					<?php 
						foreach( $menu_item['children']  as $child ) :
							output_navigation_item( $child );
						endforeach; 
					?>
				</ul>
			<?php endif; ?>

		</li>
	<?php

}//!output_navigation_item()

function output_main_navigation( $pageID ){

	?><ul class="main-menu"><?php

		$menu_structure = get_automatic_page_walker( $pageID );
		foreach( $menu_structure as $menu_item ) :
			output_navigation_item( $menu_item );
		endforeach;	
	
	?></ul><?php

}//!output_main_navigation()