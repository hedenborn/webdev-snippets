<?php
/**
 * convert_string_to_link()
 * 
 * Finds phonenumbers and emailadresses inside a given string and
 * wraps them with a tel: or mailto: link
 *
 * @param $string search for phonenumbers and emailadresses inside this string
 * @param $linkclass add a class to the wraped link
 *
 * @return string with wrapped links
 * 
 * @version 2.2
 */ 

function convert_string_to_link( $string, $linkclass = '' ){
  settype( $string, 'string' );

  //Match phone
    $phone_matches = array(
      '[+0-9]{2,5}[\-\–\s]{1,3}[0-9]{2,}[\s][0-9]{2,}[\s][0-9]{2,}[\s][0-9]{2,}',
      '[+0-9]{2,5}[\-\–\s]{1,3}[0-9]{2,}[\s][0-9]{2,}[\s][0-9]{2,}',
      '[+0-9]{2,5}[\-\–\s]{1,3}[0-9]{2,}[\s][0-9]{2,}',
      '[+0-9]{2,5}[\-\–\s]{1,3}[0-9]{5,}',
      '[+0-9]{5,}',
    );
    $phone_reg = '/';
    $phone_reg .= implode( '|', $phone_matches );
    $phone_reg .= '/';

    $string = preg_replace_callback( $phone_reg, function($match) use ($linkclass){
      $class = ( $linkclass ) ? ' class="'.$linkclass.'"' : '';
      $tel = str_replace( [' ','-','–'], '', $match[0] );
      $replacement = '<a href="tel:'.$tel.'"'.$class.'>'.$match[0].'</a>';
      return $replacement;
    }, $string );

  //Match mail
    $mail_matches = array(
      '[a-zA-ZåäaÅÄÖ0-9-_.]{2,}[\@][a-zA-ZåäaÅÄÖ0-9-_.]{2,}[\.][a-zA-ZåäaÅÄÖ0-9-_.]{2,8}',
    );
    $mail_reg = '/';
    $mail_reg .= implode( '|' , $mail_matches );
    $mail_reg .= '/';

    $string = preg_replace_callback( $mail_reg, function($match) use ($linkclass){
      $class = ( $linkclass ) ? ' class="'.$linkclass.'"' : '';
      $replacement = '<a href="mailto:'.$match[0].'"'.$class.'>'.$match[0].'</a>';
      return $replacement;
    }, $string );  

  return $string;
}