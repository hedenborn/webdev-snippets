<?php
	//Make columns sortable
	add_filter( 'manage_edit-aktivitet_sortable_columns', 'custom_taxonomies_sort' );
	function custom_taxonomies_sort( $columns ) {

		//Examples
	    $columns['taxonomy-1'] 	= 'taxonomy_name_1';
	    $columns['taxonomy-2'] 	= 'taxonomy_name_2';
	  
	    return $columns;
	}

	//Add taxonomy terms as varaibles in the orderby parameter
	function taxonomy_orderby( $clauses, $wp_query ) {
		global $wpdb;

		$order_by = '';
		if ( isset( $wp_query->query['orderby'] ) ) {
			switch ( $wp_query->query['orderby'] ) {

				//Examples
				case 'taxonomy_name_1':
					$order_by = 'taxonomy_name_1';
					break;

				case 'taxonomy_name_2':
					$order_by = 'taxonomy_name_2';
					break;

		}

		if( $order_by ){
			$clauses['join'] .= " LEFT JOIN (
				SELECT object_id, GROUP_CONCAT(name ORDER BY name ASC) AS ".$order_by."
				FROM $wpdb->term_relationships
				INNER JOIN $wpdb->term_taxonomy USING (term_taxonomy_id)
				INNER JOIN $wpdb->terms USING (term_id)
				WHERE taxonomy = '".$order_by."'
				GROUP BY object_id
			) AS ".$order_by."_terms ON ($wpdb->posts.ID = ".$order_by."_terms.object_id)";
			$clauses['orderby'] = $order_by."_terms.".$order_by." ";
			$clauses['orderby'] .= ( 'ASC' == strtoupper( $wp_query->get('order') ) ) ? 'ASC' : 'DESC';
		}

		return $clauses;
	}
	add_filter( 'posts_clauses', 'taxonomy_orderby', 10, 2 );