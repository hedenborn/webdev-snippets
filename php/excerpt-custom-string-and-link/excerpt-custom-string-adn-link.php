<?php
/**
 * apply_custom_excerpt()
 * 
 * Makes it possible to make any text into an excerpt with a custom link
 *
 * @param $content full text that will become the excerpt
 * @param $link if set adds a link at the end of the excerpt
 * @param $target if added the link gets target="_blank"
 * 
 * @return returns the excerpt
 *
 * @version 1.0
 */ 

global $excerpt_link;
global $excerpt_link_target;

function new_excerpt_more( $more ) {
  global $post;
  global $excerpt_link;
  global $excerpt_link_target;
  
  $more_tag  = '... ';
  
  if( $excerpt_link !== false ){
    $target = '';
    $read_more = __( 'Läs mer' );
    if( $excerpt_link_target !== false ){
      $target = 'target="_blank"';
    }
    $read_more = '<a class="text-link" href="'.$excerpt_link.'" '.$target.'>'.$read_more.'</a>';
    $more_tag .= $read_more;
  }

  return $more_tag;
}
function new_excerpt_length( $length ) {
    return 25;
}
function rw_trim_excerpt( $text='' )
{
    $text = strip_shortcodes( $text );
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]>', ']]&gt;', $text);
    $excerpt_length = apply_filters('excerpt_length', 'new_excerpt_more' );
    $excerpt_more = apply_filters('excerpt_more', 'new_excerpt_more' );
    return wp_trim_words( $text, $excerpt_length, $excerpt_more );
}

add_filter( 'excerpt_more', 'new_excerpt_more' );
add_filter( 'excerpt_length', 'new_excerpt_length' );
add_filter( 'wp_trim_excerpt', 'rw_trim_excerpt' );

function apply_custom_excerpt( $content, $link = false, $target = false ){
  global $excerpt_link;
  global $excerpt_link_target;
  $excerpt_link = $link;
  $excerpt_link_target = $target;
  return apply_filters( 'get_the_excerpt', $content ); 
}