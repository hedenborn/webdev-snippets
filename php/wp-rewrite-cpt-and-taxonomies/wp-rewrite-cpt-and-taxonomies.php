if (!class_exists('ObjectRewriteHandler')) :

  /**
   * Class that handles rewrites and redirects for posts and taxonomies
   *
   * Example: Post
   * Before:  /post-name
   * After:   /post-archive/category/post-name
   *
   * Example: Category
   * Before:  /categories/category
   * After:   /post-archive/category
   *
   * @version 2.0
   */
  
  class ObjectRewriteHandler {

    /*
     * Set up variables
     */

    //$key equals the taxonomy that should be changed
    //$value is the post type that will precede the affected taxonomy
    private $watch_taxonomies = [
      //'cpt_category'      => 'cpt',
      'category'          => 'post'
    ];

    //$key equals the post type that should be changed
    //$value is the taxonomy that will precede single posts from the post type
    private $watch_post_types = [
      //'cpt' => 'cpt_category',
      'post'    => 'category'
    ];


    /*
     * add hooks and filters
     */
    
    function __construct() {
      /*
       * Update rewrite when posts/taxonomies are changed
       */

      //Called when new terms are created or a term is updated
      add_action( 'edited_terms', [$this, 'add_custom_tax_post_rewrite'], 10, 2 );
      add_action( 'created_term', [$this, 'add_custom_tax_post_rewrite'], 10, 2 );

      //Called when a post is published or changed
      add_action( 'transition_post_status', [$this, 'add_custom_tax_post_rewrite'], 10, 2 );

      //Called when wp is about too alter the rewrite_rules_array
      add_filter( 'rewrite_rules_array', [$this, 'add_custom_tax_post_rewrite'] );


      /*
       * Update permalink for posts/terms
       */

      //Update term permalink
      add_filter( 'term_link', [$this, 'change_rewrited_terms_link'], 10, 3 );

      //Update post/cpt permalink
      add_filter( 'post_type_link', [$this, 'change_rewrited_posts_link'], 10, 2 );
      add_filter( 'post_link', [$this, 'change_rewrited_posts_link'], 10, 2 );


      /*
       * Redirect old post/term permalinks to new 
       */

      add_action( 'template_redirect', [$this, 'redirect_rewrited_posts_and_terms'] );
    }


    /*
     * Private functions
     */
    
    private function determine_primary_term_slug_path( $post_id, $tax_slug ) {

      $primary_term = null;
      $term_slug_path = '';
      $terms = wp_get_post_terms( $post_id, $tax_slug );

      //If yoast-seo is installed, get the primary term from WPSEO_Primary_Term
      if ( class_exists( 'WPSEO_Primary_Term' ) ) :

        $set_primary_term = new WPSEO_Primary_Term( $tax_slug, $post_id );
        $set_primary_term = $set_primary_term->get_primary_term();

        if ( $set_primary_term ) :

          $primary_term = get_term( $set_primary_term, $tax_slug );

        endif;

      endif;

      //If yoast-seo isn't installed, set the first term as primary
      if ( $terms && !isset( $terms->errors['invalid_taxonomy'] ) && !$primary_term ) :

        $primary_term = $terms[0];

      endif;

      //Continue if a primary term is found
      if ( $primary_term && !isset( $primary_term->errors['invalid_term'] ) ) : 

        $base_term = $primary_term;
        $term_slug_path = $base_term->slug;

        while( $base_term->parent ) :

          $base_term = get_term( $base_term->parent, $tax_slug );
          $term_slug_path = $base_term->slug.'/'.$term_slug_path;

        endwhile;

        $term_slug_path = $term_slug_path.'/';

      endif;

      return $term_slug_path;
    }


    private function determine_post_type_path( $post_type ) {

      //Get custom post type object for correct labels
      $cpt_obj = get_post_type_object( $post_type );

      //As stanbdard post uses the page for posts as archive for posts, get the slug of this page if so
      $post_type_slug = '';
      if ( $post_type === 'post' && $page_for_posts_id = get_option( 'page_for_posts' ) ) :

        $page_for_posts = get_post( $page_for_posts_id ); 
        $post_type_slug = $page_for_posts->post_name.'/';

      //As stanbdard WC-products uses the shop page as archive for products, get the slug of shop page if so
      elseif( $post_type === 'product' && function_exists('woocommerce_get_page_id') && $shop_id = woocommerce_get_page_id( 'shop' ) ) :

        //This part is not tested
        $page_for_posts = get_post( $shop_id ); 
        $post_type_slug = $page_for_posts->post_name.'/';

      //If post type is something other than post, use the slug label
      elseif( $cpt_obj->rewrite['slug'] ) :

        $post_type_slug = $cpt_obj->rewrite['slug'].'/';

      endif;

      return $post_type_slug;
    }



    /*
     * Public functions
     */
    
    //Filter-function that handles the rewrite of $watch_taxonomies and $watch_post_types
    public function add_custom_tax_post_rewrite( $rules, $is_tax_filter = false ) {

      //$rules is only used if the function is called by rewrite_rules_array
      //If the function is called by edited_terms or created_term $rules = $term_id and $is_tax_filter = $taxonomy
      //If the function is called by transition_post_status $rules = $new_status and $is_tax_filter = $old_status
      //Therfore $is_tax_filter will be true if called by any other filter than rewrite_rules_array

      global $wp_rewrite;
      $rewrites = [];


      /*
       * Rewrite for terms
       */
      foreach ( $this->watch_taxonomies as $tax_slug => $post_type ) :

        $post_type_slug = $this->determine_post_type_path( $post_type );

        //Get all terms that should be rewrited for current taxonomy
        $terms = get_terms( $tax_slug, [
          'hide_empty' => false,
        ]);

        if ( $terms ) :
          foreach ( $terms as $term_obj ) : 

            $term_slug = $term_obj->slug;
            $term_parent = $term_obj->parent;

            while ( $term_parent ) :

              $parent_obj = get_term_by( 'id', $term_parent, $tax_slug );
              $term_slug = $parent_obj->slug.'/'.$term_slug;
              $term_parent = $parent_obj->parent;

            endwhile;

            $regex_rule = '^'.$post_type_slug.$term_slug.'?$';
            $redirect = 'index.php?taxonomy='.$tax_slug.'&term='.$term_obj->slug;

            //Add rewrite rule in array for later use
            $rewrites[$regex_rule] = $redirect;

          endforeach;
        endif; 
        
      endforeach; 


      /*
       * Rewrite for posts
       */
      foreach ( $this->watch_post_types as $post_type => $tax_slug ) :

        $post_type_slug = $this->determine_post_type_path( $post_type );

        //Get all posts that should be rewrited
        $posts_array = get_posts([
          'posts_per_page' => -1,
          'post_type'      => $post_type
        ]);

        foreach ( $posts_array as $post ) :

          $term_slug = $this->determine_primary_term_slug_path( $post->ID, $tax_slug );
          $regex_rule = '^'.$post_type_slug.$term_slug.$post->post_name.'?$';
          $redirect = 'index.php?'.( ($post_type === 'post') ? 'name' : $post_type ).'='.$post->post_name;

          //Add rewrite rule in array for later use
          $rewrites[$regex_rule] = $redirect;

        endforeach;

      endforeach;


      //Save rules
      if ( $is_tax_filter ) : 

        //Add rule with add_rewrite_rule if action edited_terms or created_term
        foreach ( $rewrites as $regex_rule => $redirect ) :
          add_rewrite_rule( $regex_rule, $redirect, 'top' );
        endforeach;

        $wp_rewrite->flush_rules();

      else :

        //Add rules with $rules if is filter rewrite_rules_array
        $rules = array_reverse( $rules, true );

        foreach ( $rewrites as $regex_rule => $redirect ) :
          $rules[$regex_rule] = $redirect;
        endforeach;

        $rules = array_reverse( $rules, true );

        return $rules;

      endif;
    }


    //Filter-function that changes the term_link for all terms connected to $watch_taxonomies
    public function change_rewrited_terms_link( $url, $term_obj, $tax_slug ) {

      if ( array_key_exists( $tax_slug, $this->watch_taxonomies ) ) {
        
        $post_type = $this->watch_taxonomies[$tax_slug];
        $post_type_slug = $this->determine_post_type_path( $post_type );

        $term_slug = $term_obj->slug;
        $term_parent = $term_obj->parent;

        while ( $term_parent ) :

          $parent_obj = get_term_by( 'id', $term_parent, $tax_slug );
          $term_slug = $parent_obj->slug.'/'.$term_slug;
          $term_parent = $parent_obj->parent;

        endwhile;

        $new_path = '/'.$post_type_slug.$term_slug.'/';
        $url = get_home_url().$new_path;

      }

      return $url;
    }


    //Filter-function that changes the permalink for all posts connected to $watch_post_types
    public function change_rewrited_posts_link( $url, $post ) {

      if ( array_key_exists( $post->post_type, $this->watch_post_types ) ) { 

        $tax_slug = $this->watch_post_types[$post->post_type];
        $post_type_slug = $this->determine_post_type_path( $post->post_type );
        $term_slug = $this->determine_primary_term_slug_path( $post->ID, $tax_slug );

        $new_path = '/'.$post_type_slug.$term_slug.$post->post_name.'/';
        $url = get_home_url().$new_path;

      }

      return $url;
    }


    //Redirect for rewrited posts and taxonomies
    public function redirect_rewrited_posts_and_terms() {

      return $this->watch_taxonomies;

      $tax_array = array_keys( $this->watch_taxonomies );
      $pt_array = array_keys( $this->watch_post_types );
      $current_url = $_SERVER['REQUEST_URI'];

      //If is rewrited taxonomy
      if ( is_tax( $tax_array ) ) :

        $term = get_queried_object();
        $link = get_term_link( $term );
        $path = str_replace( get_home_url(), '', $link );

        if ( $current_url !== $path ) :

          wp_safe_redirect( $path, 301 );
          exit;

        endif;

      //If is rewrited post
      elseif ( is_singular( $pt_array ) ) :

        $post = get_post();
        $link = get_permalink( $post );
        $path = str_replace( get_home_url(), '', $link );

        if ( $current_url !== $path ) :

          wp_safe_redirect( $path, 301 );
          exit;

        endif;

      endif;
    }
  }

  $objectRewriteHandler = new ObjectRewriteHandler();

endif; //!class_exists('ObjectRewriteHandler')