<?php 

$instagramID = '505286598';

if( defined('WP_DEBUG') && true === WP_DEBUG ){

	$instagram_json = get_template_directory_uri().'/assets/scripts/instagram_testfeed.json';

} else {

	$instagram_json = 'http://instagram/'.$instagramID;

}

$instaFeed = json_decode( @file_get_contents( $instagram_json ), true);


?>

<?php 
if( isset($instaFeed['data']) && $instaFeed['data'] ) :
	foreach( $instaFeed['data'] as $key => $instaItem ) : 
		?>

	<div class="instagram-image">
		<a href="<?php echo $instaItem['link']; ?>" target="_blank" title="Öppna i Instagram">
			<img src="<?php echo $instaItem['images']['standard_resolution']['url']; ?>" alt="<?php echo $instaItem['caption']['text']; ?>">
		</a>
	</div>


	<?php 
	if ( $key == 5 ) :
		break;
	endif;
	?>

<?php 
	endforeach; 
endif;
?>