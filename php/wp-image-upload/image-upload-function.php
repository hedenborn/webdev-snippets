<?php
	if ( ! function_exists( 'wp_handle_upload' ) ) {
		//Alter path for your theme
	    require_once( dirname(__FILE__) . '/../../../wp-load.php' );
	    require_once( dirname(__FILE__) . '/../../../wp-admin/includes/file.php' );
	    require_once( dirname(__FILE__) . '/../../../wp-admin/includes/image.php' );
	}

	//get the uploaded file
	$uploadedfile = $_FILES['file'];

	//Check filetype
	$allowed =  array( 'png' ,'jpg', 'jpeg' );
	$ext = pathinfo( $uploadedfile['name'], PATHINFO_EXTENSION);
	if( in_array( $ext, $allowed) ) {

		//Upload file
		$upload_overrides = array( 'test_form' => false );
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

		if ( $movefile && ! isset( $movefile['error'] ) ) {

			//Create media attachment
			$filepath = $movefile['url'];
			$filetype = wp_check_filetype( basename( $filepath ), null );
			$wp_upload_dir = wp_upload_dir();
			$attachment = array(
				'guid'           => $wp_upload_dir['url'] . '/' . basename( $filepath ), 
				'post_mime_type' => $filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filepath ) ),
				'post_content'   => '',
			);

			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $filepath, 0 );

			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filepath );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			//Save data to callback
		    $image_data['status'] = true;
		    $image_data['data'] = $movefile;
		    $image_data['attach_id'] = $attach_id;
		   
		} else {
		    //Save data to callback
		    $image_data['status'] = false;
		    $image_data['text'] = $movefile['error'];
		    $image_data['data'] = $movefile;
		}

	}else{
		//Save data to callback
		$image_data['status'] = false;
		$image_data['text'] = 'Filtyp är inte tillåten';
	}

	echo json_encode([ 'image_data' => $image_data ]);
	die();