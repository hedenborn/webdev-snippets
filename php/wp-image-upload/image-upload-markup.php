<form data-id="<?= get_the_ID(); ?>" enctype="multipart/form-data" class="edit-staff-form form-edit form-edit--card" method="POST">
	<div class="row">

		<div class="col-xs-6">
			<div class="input-container">
				<label for="title">Titel</label>
				<input class="staff-edit__item staff-edit-title" type="text" name="title" value="<?= $title; ?>">
			</div>
		</div>

		<div class="col-xs-6">
			<div class="input-container">
				<label for="email">Email</label>
				<input class="staff-edit__item staff-edit-email" type="text" name="email" value="<?= $email; ?>">
			</div>
		</div>

		<div class="col-xs-6">
			<div class="input-container">
				<label for="phone">Telefon</label>
				<input class="staff-edit__item staff-edit-phone" type="text" name="phone" value="<?= $phone; ?>">
			</div>
		</div>

		<div class="col-xs-6">
			<div class="input-container">
				<label for="mobile">Mobil</label>
				<input class="staff-edit__item staff-edit-mobile" type="text" name="mobile" value="<?= $mobile; ?>">
			</div>
		</div>

		<div class="col-xs-12">
			<div class="input-container">
				<input class="staff-edit__item staff-edit-image" type="file" name="image" value="">
				<label for="mobile">Ladda upp bild</label>
			</div>
		</div>

		<div class="col-xs-12">
			<div class="input-container">
				<label for="cities">Välj orter</label>

				<select class="staff-edit__item staff-edit-cites input-select-city" value="<?= implode( ',', $check_cities ); ?>" name="cities" multiple data-placeholder="Välj orter">

				    <?php 
				    	foreach( $terms as $term ) :
					    	$name = $term->name;
					    	$slug = $term->slug;
					    	$selected = ( in_array( $term->slug, $check_cities ) ) ? ' selected' : '';
				    ?>
				    	<option value="<?= $slug ?>"<?= $selected ?>><?= $name ?></option>

				    <?php endforeach; ?>

				 </select>
				 
			</div>
		</div>
		
		<div class="col-xs-12">
			<div class="input-container pad-0 text-left">
				<input class="staff-edit__btn" type="submit" value="Uppdatera">
			</div>
		</div>	

	</div>


</form>