<?php
	function update_staff(){

		$response = $_POST['data'];
		$success = true;

		if( isset( $response['image'] ) && $response['image'] ){
			update_field( 'field_598c3de5e6ca5', $response['image'], $response['ID'] );
		}

		$terms = [];
		$city_names = [];
		if( $response['cities'] ){
			foreach( $response['cities'] as $city ) :
				$temp = get_term_by( 'slug', $city, 'city' );
				$terms[] = $temp->term_id;
				$city_names[] = $temp->name;
			endforeach;
		} 

		wp_set_post_terms( $response['ID'], $terms , 'city', false );

		update_field( 'field_598c619bad01a', $response['title'], $response['ID'] );
		update_field( 'field_598c61a5ad01b', $response['phone'], $response['ID'] );
		update_field( 'field_598c6216ad01c', $response['mobile'], $response['ID'] );
		update_field( 'field_598c621bad01d', $response['email'], $response['ID'] );

		echo json_encode([ 'success' => $success, 'cities' => implode( ' ', $city_names ) ]);

		die();
	}

	add_action("wp_ajax_update_staff", "update_staff");
	add_action("wp_ajax_nopriv_update_staff", "update_staff");