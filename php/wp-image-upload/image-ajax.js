/*==============================================================================
  # AJAX Update staff post_type
  ==============================================================================*/

  function update_staff_ajax( dataObj, output_messages ){

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: my_ajax_object.ajax_url,
      data: { action: 'update_staff', data : dataObj },
      success: function(response){

        if( response.success ) {
          $('#staff-card-'+dataObj.dataID).find('.staff-title').text(dataObj.title);
          $('#staff-card-'+dataObj.dataID).find('.staff-phone').text(dataObj.phone);
          $('#staff-card-'+dataObj.dataID).find('.staff-mobile').text(dataObj.mobile);
          $('#staff-card-'+dataObj.dataID).find('.staff-email').html('<a href="mailto:'+ dataObj.email +'">'+ dataObj.email +'</a>');
          $('#staff-card-'+dataObj.dataID).find('.card__inner__cities').text( response.cities );
          $('#staff-card-'+dataObj.dataID).find('.thumb').css( 'background-image', dataObj.image_data.data.url );

          output_messages.push({
            text: 'Uppdateringen lyckades',
            status: 'sucess',
          });

        }else{

          output_messages.push({
            text: 'Uppdateringen misslyckades',
            status: 'fail',
          });

        }

        if( output_messages.length > 0 ){
          output_message( output_messages );
        }

      }
    }); 

  }

  $('.edit-staff-form').on('submit', function(e) {  

    var file_data = $(this).find('.staff-edit-image').prop('files')[0],
        title = $(this).find('.staff-edit-title').val(),
        phone = $(this).find('.staff-edit-phone').val(),
        mobile = $(this).find('.staff-edit-mobile').val(),
        email = $(this).find('.staff-edit-email').val(),
        cities = $(this).find('.staff-edit-cites').val(),
        dataID = $(this).attr('data-id'),
        output_messages = [];

    e.preventDefault();

    var data = {
      name: name,
      title: title,
      phone: phone,
      mobile: mobile,
      email: email,
      cities: cities,
      ID: dataID,
    };

    if( file_data ){

      var form_data = new FormData(); 
      form_data.append('file', file_data);

      //Upload image
      $.ajax({
        url: '/wp-content/themes/opg-portal/upload_image.php',
        data : form_data,
        dataType: 'json',
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(response){
  
          if( response.image_data.status ){

            output_messages.push({
              text: 'Bilden laddades upp',
              status: 'sucess',
            });

            data.image = response.image_data.attach_id;

          }else{
            
            if( response.image_data.text ){

              output_messages.push({
                text: response.image_data.text,
                status: 'fail',
              });

            }else{

              output_messages.push({
                text: 'Bilden kunde inte laddas upp',
                status: 'fail',
              });

            }
          }

          update_staff_ajax( data, output_messages  );

        } //<!--success-->

      }); //<!--ajax-->

    }else{

      update_staff_ajax( data, output_messages );

    }

  });