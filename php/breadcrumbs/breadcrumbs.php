<?php
/**
 * output_breadcrumbs()
 * 
 * Detects page_type and outputs breadcrumbs accordingly
 * 
 * @return outputs a div.breadcrumb-container containing breadcrumbs
 *
 * @version 1.1
 */ 

function output_breadcrumbs(){

	$breadcrumbs[] = ( is_front_page() ) ? '<span>'.get_the_title().'</span>' : '<span><a href="'.get_home_url().'">'.get_the_title( get_option('page_on_front') ).'</a></span>';

	if( is_page() ) :

		$post_ancestors = get_post_ancestors( $post );
		if( $post_ancestors ) :
			$post_ancestors = array_reverse( $post_ancestors );
			foreach ( $post_ancestors as $post_ancestor ) :
				$breadcrumbs[] = '<span><a href="'.get_the_permalink( $post_ancestor ).'">'.get_the_title( $post_ancestor ).'</a></span>';
			endforeach;
		endif;
		$breadcrumbs[] = '<span>'.get_the_title().'</span>';

	elseif( is_single() ) :

		$post_type = get_post_type();
		$post_type_obj = get_post_type_object( $post_type );
		if( $post_type_obj->has_archive ) : 
			$breadcrumbs[] = '<span><a href="'.get_post_type_archive_link( $post_type ).'">'.$post_type_obj->labels->name.'</a></span>';
		endif;
		$breadcrumbs[] = '<span>'.get_the_title().'</span>';

	elseif( is_tax() ) :

		global $wp_taxonomies;
		$queried_object = get_queried_object();
		$tax_post = ( isset( $wp_taxonomies[$queried_object->taxonomy] ) ) ? $wp_taxonomies[$queried_object->taxonomy]->object_type : array();

		if( isset( $tax_post[0] ) && $tax_post[0] ) :
			$post_type_obj = get_post_type_object( $tax_post[0] );
			$breadcrumbs[] = '<span><a href="'.get_post_type_archive_link( $tax_post[0] ).'">'.$post_type_obj->labels->name.'</a></span>';
		endif;

		$ancestors = get_ancestors( $queried_object->term_id, $queried_object->taxonomy, 'taxonomy' );

		if( $ancestors ) :
			array_reverse( $ancestors );
			foreach( $ancestors as $ancestor ) :
				$term = get_term_by( 'id', $ancestor, $queried_object->taxonomy );
				$breadcrumbs[] = '<span><a href="'.get_term_link( $term ).'">'.$term->name.'</a></span>';
			endforeach;
		endif;

		$breadcrumbs[] = '<span>'.$queried_object->name.'</span>';


	elseif( is_archive() ) :

		$post_type = get_post_type();
		$post_type_obj = get_post_type_object( $post_type );
		$breadcrumbs[] = '<span>'.$post_type_obj->labels->name.'</span>';

	elseif ( is_search() ) : 

		$search_crumb = '<span>S�kresultat';
		if( get_search_query() ){
			$search_querry = esc_attr( get_search_query() );
			$search_crumb .= ' f�r: '.$search_querry;
		}
		$search_crumb .= '</span>';
		$breadcrumbs[] = $search_crumb;

	elseif ( is_404() ) : 

		$breadcrumbs[] = '<span>Sida hittades ej</span>';

	else :
		$breadcrumbs[] = '<span>'.get_the_title().'</span>';

	endif; 

	echo '<div class="breadcrumb-container">'.implode(' <span class="divider">/</span> ', $breadcrumbs ).'</div>';
}