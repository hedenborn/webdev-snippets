<?php
/**
 * Template Name: Progressively Collapsable Menu
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>
<?php endwhile; ?>




<header id="pcm" class="okayNav-header">
	
	<a class="pcm-site-logo" href="#">
		<img src="<?php echo get_template_directory_uri() ?>/dist/images/oas.png" alt="Oas">
	</a>

	<nav role="navigation" id="nav-main" class="okayNav">

		<?php
		wp_nav_menu([
			'theme_location' => 'pro-collapse', 
			'menu_class' => '',
		]);
		?>

	</nav>
</header>

<div class="container">
	<div class="row">
		<div class="pcm-text-container">
			<p class="pcm-text">Förminska fönstret för att se effekten. hörrö! gör det.</p>
		</div>
	</div>
</div>








